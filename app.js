const express = require('express')
const cors = require('cors')
const logger = require('morgan')

// const bodyParser = require('body-parser')
const app = new express()


app.use(cors()) 
app.use(express.json())
// app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.urlencoded({extended:true}))
app.use(logger('dev'))
require('dotenv').config()
require('./middlewares/mongoDB')

const PORT = process.env.PORT || 3000;
const { Server } = require("socket.io");

const io = new Server({ /* options */ });

io.on("connection", (socket) => {
  // ...
  console.log("New user ", socket.id);
  socket.on('msg', (msg, room)=>{
    console.log(msg);
    console.log(room);
    io.emit('new', `message received : ${msg.msg}`)
  })
  
});

io.listen(3001)


app.use('/uploads', express.static('uploads'));

 
const userApi = require('./routes/user')
app.use('/user', userApi)
const adminApi = require('./routes/admin')
app.use('/admin', adminApi)
require('./routes/cron')




app.listen(PORT, ()=>{ 
    console.log(`-------------Server running on PORT ${PORT} ---------------`);
}) 