const express = require('express')
const multer = require('multer');
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
require('dotenv').config()
const router = express.Router()

const saltRounds = 10;

const userModel = require('../models/userModel')

// create multer storage----------
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads/')
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname)
    }
})

// multer upload instance
const upload = multer({ storage: storage })



//  User Registration 
router.post('/register', upload.single('image'), async (req, res) => {
    try {
        req.body.image = req.file.path
        let data = req.body
        let password = data.password

        // password encryption
        bcrypt.hash(password, saltRounds, async (err, hash) => {
            if (err) {
                console.log(err);
            } else {
                console.log(hash);
                data.password = hash
                console.log(data.password);
                const newUser = await new userModel(data)
                await newUser.save()
            }
        })
        res.send({ "status": "successfully created" }).status(201)

    } catch (error) {

        console.log(error);
        res.status(500).json({ "status": error.message })
    }

})

// User Login
router.post('/login', async (req, res) => {
    try {
        let data = req.body.data
        console.log(data);
        let email = data.email
        let password = data.password
        let dataFromDB = await userModel.findOne({ email: email })
        console.log("1 ", dataFromDB);

        if (dataFromDB != null) {
            console.log("from db ", dataFromDB);

            // comparing plain password with encrypted password from db
            bcrypt.compare(password, dataFromDB.password, (err, result) => {

                if (err) {
                    console.log(err);
                    res.status(500).json({ "status": "error" })
                }
                 else {
                    console.log(result);

                    if (!result) {
                        console.log("Invalid credentials")
                        res.json({ "status": "Invalid credentials" }).status(404)
                    }
                    else {
                        console.log("Login successful");
                        
                        //  token generation for successful login
                        let payload = {
                            email : email,
                            password : dataFromDB.password
                        }
                        let token = jwt.sign(
                            payload, process.env.SECRET, {
                                expiresIn : "1d"
                            }
                        )
                        res.json([{ "status": "login successful" },token]).status(200)
                    }
                }
            })
        }
        else {
            console.log("Account doesn't exists")
            res.json({ "status": "account doesn't exists" }).status(404)
        }

    } catch (error) {

        console.log(error);
        res.status(500).json({ "status": "error" })
    }
})



module.exports = router