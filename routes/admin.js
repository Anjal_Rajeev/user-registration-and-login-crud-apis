const express = require('express')
const router = express.Router()

const userModel = require('../models/userModel')
const verifyToken = require('../middlewares/jwtVerify')

//  Listing all users
router.get('/users',verifyToken, async (req, res) => {
    try {

        // -------pagination----------
        let page = parseInt(req.query.page)
        let limit = 5

        let startIndex = (page - 1) * limit

        // -------filtering & sorting-----------
        let userName = req.query.name
        let email = req.query.email
        let ageFrom = parseInt(req.query.ageFrom)
        let ageTo = parseInt(req.query.ageTo)
        let data

        if (userName || email || (ageFrom && ageTo)) {

            // for filtered listing
            // data = await userModel.find({ $or:[{email : email},{userName : userName}]}).sort({createdAt : -1})
            data = await userModel.aggregate([
                { $match: 
                    { $or: [
                        { email: email }, 
                        { userName: userName }, 
                        { $and: [
                            { age: { $gte: ageFrom } }, 
                            { age: { $lte: ageTo } }
                            ] 
                        }] 
                    } 
                },
                { $sort :{ age : 1}},
                { $skip : startIndex},
                { $limit : limit},
                { $project : {password : 0, __v : 0, createdAt : 0, updatedAt : 0}}

            ])

        } else {

            // for normal listing
            console.log("empty");
            data = await userModel.aggregate([
                { $sort : {createdAt : -1}},
                { $skip : startIndex},
                { $limit : limit},
                { $project : { password : 0, __v : 0, createdAt : 0, updatedAt : 0}}
            ])

            // data = await userModel.find().limit(limit).skip(startIndex).sort({createdAt : -1})
        }

        console.log(data);
        res.send(data).status(200)

    } catch (error) {

        console.log(error);
        res.status(500).json({ "status": "error" })
    }
})

//  Listing single user
router.get('/user/:id', verifyToken, async (req, res) => {
    try {
        let id = req.params.id
        let data = await userModel.findOne({ _id: id })
        console.log(data);
        res.send(data).status(200)

    } catch (error) {

        console.log(error);
        res.status(500).json({ "status": "error" })
    }
})

// Edit user
router.put('/user/:id', verifyToken, async (req, res) => {
    try {
        let id = req.params.id
        let data = req.body
        console.log("from frontend", data);
        let edited = await userModel.findByIdAndUpdate(id, data, { new: true })
        console.log("edited", edited);
        if (edited != null) {
            res.send({ "status": " edited" }).status(200)
        }
        else {
            res.status(404).json({ "status": "user doesn't found" })
        }


    } catch (error) {

        console.log(error);
        res.status(500).json({ "status": "error" })
    }
})

//  Delete user
router.delete('/user/:id', verifyToken, async (req, res) => {
    try {
        let id = req.params.id
        let deleted = await userModel.findByIdAndDelete(id)
        if (deleted != null) {
            res.send({ 'status': "deleted" }).status(200)
        }
        else {
            res.status(404).json({ "status": "user doesn't found" })
        }


    } catch (error) {

        console.log(error);
        res.status(500).json({ "status": "error" })
    }
})

router.post('/user/test', async (req, res)=>{
    try {
        console.log(req.body);
        res.send(req.body) 
    } catch (error) {
        console.log(error);
        res.status(500).json({ "status": "error" })
    }
})



module.exports = router