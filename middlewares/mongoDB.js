const mongoose = require ('mongoose')
require('dotenv').config()
mongoose.set('strictQuery', true);

const MONGO_URL = process.env.MONGO_URL



mongoose.connect(MONGO_URL)
.then(()=>{
    console.log('-------mongodb connected successfully-------')
})
.catch((error)=>{
    console.log(error.message)
})  