const jwt = require("jsonwebtoken");
require('dotenv').config()

const verifyToken = (req, res, next) => {

    const fullToken = req.headers.authorization

    if (!fullToken) {

        return res.status(401).send('Unauthorized request')
    }
    else if (fullToken === null) {

        return res.status(401).send('Unauthorized request')
    } 
    else {

        let token = fullToken.split(" ")
        token = token[1]
        console.log(token);
        jwt.verify(token, process.env.SECRET, (err, payload) => {
            
            if (err) {
                return res.status(401).send('Unauthorized request')
            }
            next()
        })
    }



}



module.exports = verifyToken